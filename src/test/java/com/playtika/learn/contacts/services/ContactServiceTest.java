package com.playtika.learn.contacts.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import com.playtika.learn.contacts.Contact;
import lombok.SneakyThrows;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.net.http.HttpClient;
import java.util.ArrayList;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@WireMockTest(httpPort = 8080)
class ContactServiceTest {
    private ContactService contactService;

    @BeforeEach
    void init() {
        contactService = new ContactService(
                new ObjectMapper(),
                HttpClient.newBuilder().build(),
                "http://localhost:8080");
    }

    @Test
    void shouldGetContacts() {
       stubFor(WireMock.get(urlEqualTo("/contacts")).willReturn(okJson(getStubContactsString())));

       var gotContacts = contactService.getContacts();

       assertAll(
               ()-> assertThat(gotContacts.size()).isEqualTo(3),
               ()-> assertThat(gotContacts.get(0).getFullName()).isEqualTo("oleh skrypka"),
               ()-> assertThat(gotContacts.get(1).getFullName()).isEqualTo("viktor pavlik")
       );
    }

    @ParameterizedTest
    @ValueSource(longs = { 1L,2L,3L })
    void shouldGetContact(long id) {
        stubFor(WireMock.get(urlEqualTo("/contacts/" + id)).willReturn(okJson(getStubContactString(id))));

        var gotContact = contactService.getContact(id);

        assertThat(gotContact.getId()).isEqualTo(id);
    }

    @Test
    void shouldAddContacts() {
        stubFor(WireMock.post(urlEqualTo("/contacts"))
                .withRequestBody(equalToJson(getStubContactsString()))
                .willReturn(ok()));

        contactService.addContacts(getStubContacts());

        WireMock.verify(postRequestedFor(urlEqualTo("/contacts"))
                .withRequestBody(equalToJson(getStubContactsString())));
    }

    private List<Contact> getStubContacts() {
        List<Contact> contacts = new ArrayList<>();

        contacts.add(new Contact("oleh", "skrypka", List.of("oleh.s@mail.com"), List.of(100000L),1L));
        contacts.add(new Contact("viktor", "pavlik", List.of("vpavlik@mail.com"), List.of(200000L),2L));
        contacts.add(new Contact("ruslana", "pysanka", List.of("psy@mail.com"), List.of(300000L),3L));

        return contacts;
    }

    @SneakyThrows
    private String getStubContactsString() {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.writeValueAsString(getStubContacts());
    }

    @SneakyThrows
    private String getStubContactString(long id) {
        ObjectMapper mapper = new ObjectMapper();
        var contact = getStubContacts().get(0);
        contact.setId(id);

        return mapper.writeValueAsString(contact);
    }
}