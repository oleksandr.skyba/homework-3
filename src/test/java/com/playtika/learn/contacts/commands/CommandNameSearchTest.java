package com.playtika.learn.contacts.commands;

import com.playtika.learn.contacts.ContactsContainer;
import com.playtika.learn.contacts.exceptions.QuitException;
import com.playtika.learn.contacts.io.Input;
import com.playtika.learn.contacts.io.Output;
import lombok.SneakyThrows;
import org.javatuples.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static com.playtika.learn.contacts.TestHelper.createContacts;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CommandNameSearchTest {

    @Mock
    Input input;

    @Mock
    Output output;

    @Mock
    ContactsContainer contactsContainer;

    CommandNameSearch command;

    @BeforeEach
    void init() {
        when(input.getQuitWord()).thenReturn("quit");

        command = new CommandNameSearch(input, output);
    }

    @Test
    void shouldGetCommandId() {
        assertThat(command.getCommandId()).isEqualTo(CommandId.SEARCH_BY_NAME);
    }

    @Test
    @SneakyThrows
    void shouldFindTwoContacts() {
        when(input.getString()).thenReturn(new Pair<>("kapl", false));

        when(contactsContainer.getContacts(any()))
                .thenReturn(createContacts(List.of(
                        new Pair<>("anna", "kaplun"),
                        new Pair<>("serhii", "kaplan"))));

       var container = command.act(contactsContainer);

       verify(output, times(1)).printMessage("%d %s", 1L, "anna kaplun");
       verify(output, times(1)).printMessage("%d %s", 2L, "serhii kaplan");
    }

    @Test
    @SneakyThrows
    void shouldFindTwoContactsAfterSecondTry() {
        when(input.getString())
                .thenReturn(new Pair<>("", true))
                .thenReturn(new Pair<>("kapl", false));

        when(contactsContainer.getContacts(any()))
                .thenReturn(createContacts(List.of(
                        new Pair<>("anna", "kaplun"),
                        new Pair<>("serhii", "kaplan"))));

        var container = command.act(contactsContainer);

        verify(output, times(1)).printMessage(startsWith("Enter valid chars"), anyString());
        verify(output, times(1)).printMessage("%d %s", 1L, "anna kaplun");
        verify(output, times(1)).printMessage("%d %s", 2L, "serhii kaplan");
    }

    @Test
    @SneakyThrows
    void shouldGetQuitException() {
        when(input.getString()).thenThrow(new QuitException());

        var container = command.act(contactsContainer);

        verify(output, never()).printMessage(contains("%d %s"), anyLong(), anyString());
        verify(contactsContainer, never()).getContacts(any());
    }

    @Test
    void shouldGetCommandDescription() {
        assertThat(command.getCommandDescription()).isEqualTo("Search contacts by name");
    }
}