package com.playtika.learn.contacts.commands;

import com.playtika.learn.contacts.Contact;
import com.playtika.learn.contacts.ContactsContainer;
import com.playtika.learn.contacts.exceptions.QuitException;
import com.playtika.learn.contacts.io.Input;
import com.playtika.learn.contacts.io.Output;
import lombok.SneakyThrows;
import org.javatuples.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CommandDetailsTest {

    @Mock
    Input input;

    @Mock
    Output output;

    @Mock
    ContactsContainer contactsContainer;

    CommandDetails command;

    @BeforeEach
    void init() {
        when(input.getQuitWord()).thenReturn("quit");

        command = new CommandDetails(input, output);
    }

    @Test
    @SneakyThrows
    void shouldShowContact() {
        when(input.getLong()).thenReturn(new Pair<>(111L, false));

        when(contactsContainer.getContact(anyLong()))
                .thenReturn(new Contact(
                        "name",
                        "surname",
                        List.of("ha@mail.com"),
                        List.of(12345678L),
                        111L));

        var updatedContacts = command.act(contactsContainer);

        verify(contactsContainer).getContact(111L);

        verify(output, atLeastOnce()).printMessage(startsWith("Name: "), anyString());
        verify(output, atLeastOnce()).printMessage(startsWith("Surname: "), anyString());
        verify(output, atLeastOnce()).printMessage(startsWith("Id: "), anyLong());
        verify(output, atLeastOnce()).printMessage(startsWith("Emails: "), anyString());
        verify(output, atLeastOnce()).printMessage(startsWith("Phones: "), anyString());
    }

    @SneakyThrows
    @Test
    void shouldNotShowContactAndPrintError() {
        when(input.getLong())
                .thenReturn(new Pair<>(0L, true))
                .thenThrow(new QuitException());

        var updatedContacts = command.act(contactsContainer);

        verify(output, atLeastOnce()).printMessage(startsWith("Enter correct contact id"), anyString());
    }

    @Test
    void shouldGetCommandId() {
        assertThat(command.getCommandId()).isEqualTo(CommandId.DETAILS);
    }

    @Test
    void shouldGetCommandDescription() {
        assertThat(command.getCommandDescription()).isEqualTo("Show contact details");
    }


}