package com.playtika.learn.contacts;

import lombok.SneakyThrows;
import org.javatuples.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.playtika.learn.contacts.TestHelper.createContacts;
import static org.assertj.core.api.Assertions.assertThat;

class ContactsBookTest {

    ContactsBook contactsBook;

    @BeforeEach
    void init() {
        contactsBook = new ContactsBook();
    }

    @Test
    @SneakyThrows
    void shouldAddContactAndReturnId() {
        Contact contact = new Contact();

        long contactId = contactsBook.addContact(contact);
        Thread.sleep(1L);

        Instant nowInstant = Instant.now();
        Long timestampId = nowInstant.toEpochMilli();

        assertThat(contactId).isLessThan(timestampId);
        assertThat(contactId).isGreaterThan(0L);
        assertThat(contactsBook.getAllContacts().isEmpty()).isFalse();
    }

    @Test
    @SneakyThrows
    void shouldAddContactAndSetItsId() {
        Contact contact = new Contact();

        long contactId = contactsBook.addContact(contact);
        Thread.sleep(1L);

        Instant nowInstant = Instant.now();
        Long timestampId = nowInstant.toEpochMilli();

        assertThat(contactsBook.getAllContacts().get(0).getId()).isLessThan(timestampId);
        assertThat(contactsBook.getAllContacts().get(0).getId()).isGreaterThan(0L);
    }

    @Test
    @SneakyThrows
    void getAllContacts() {

        for (var contact: createContacts(5)) {
            contactsBook.addContact(contact);
            Thread.sleep(1L);
        }

        assertThat(contactsBook.getAllContacts().size()).isEqualTo(5);
    }

    @Test
    void getAllContactsEmptyList() {

        assertThat(contactsBook.getAllContacts().size()).isEqualTo(0);
    }

    @Test
    @SneakyThrows
    void getContactsById() {
        List<Long> timestamps = new ArrayList<>();
        for (var contact: createContacts(5)) {
            long timestamp = contactsBook.addContact(contact);
            timestamps.add(timestamp);
            Thread.sleep(1L);
        }

        var foundContacts = contactsBook.getContacts(contact -> contact.getId() < timestamps.get(3));

        assertThat(foundContacts.size()).isEqualTo(3);
    }

    @Test
    @SneakyThrows
    void getContactsByFullName() {
        var contacts = createContacts(List.of(
                new Pair<>("anna", "kaplun"),
                new Pair<>("serhii", "kaplan"),
                new Pair<>("oleh", "taran"),
                new Pair<>("inna", "tatarova")));

        for (var contact: contacts) {
            contactsBook.addContact(contact);
            Thread.sleep(1L);
        }

        var foundContacts = contactsBook.getContacts(contact -> {
            return contact.getFullName().toLowerCase(Locale.ROOT).contains("kapl");
        });

        assertThat(foundContacts.size()).isEqualTo(2);
    }

    @Test
    void getContactsByFullNameNotFound() {
        var contacts = createContacts(List.of(
                new Pair<>("anna", "kaplun"),
                new Pair<>("serhii", "kaplan"),
                new Pair<>("oleh", "taran"),
                new Pair<>("inna", "tatarova")));

        for (var contact: contacts) {
            contactsBook.addContact(contact);
        }

        var foundContacts = contactsBook.getContacts(contact -> {
            return contact.getFullName().toLowerCase(Locale.ROOT).contains("sat");
        });

        assertThat(foundContacts.size()).isEqualTo(0);
    }

    @Test
    @SneakyThrows
    void deleteContact() {
        List<Long> timestamps = new ArrayList<>();
        for (var contact: createContacts(5)) {
            long timestamp = contactsBook.addContact(contact);
            timestamps.add(timestamp);
            Thread.sleep(1L);
        }

        contactsBook.deleteContact(timestamps.get(1));

        assertThat(contactsBook.getAllContacts().size()).isEqualTo(4);
        assertThat(contactsBook.getAllContacts().stream()
                .filter(contact -> contact.getId().equals(timestamps.get(1)))
                .findAny()).isEmpty();
    }

    @Test
    @SneakyThrows
    void deleteContactContactNotDeleted() {
        List<Long> timestamps = new ArrayList<>();
        for (var contact: createContacts(5)) {
            long timestamp = contactsBook.addContact(contact);
            timestamps.add(timestamp);
            Thread.sleep(1L);
        }

        long timestamp = timestamps.get(0) - 1;
        contactsBook.deleteContact(timestamp);

        assertThat(contactsBook.getAllContacts().size()).isEqualTo(5);
        assertThat(contactsBook.getAllContacts().stream()
                .filter(contact -> contact.getId().equals(timestamp))
                .findAny()).isEmpty();
    }

    @Test
    @SneakyThrows
    void getContact() {

        List<Long> timestamps = new ArrayList<>();
        for (var contact: createContacts(5)) {
            long timestamp = contactsBook.addContact(contact);
            timestamps.add(timestamp);
            Thread.sleep(1L);
        }

        long timestamp = timestamps.get(3);
        Contact contact = contactsBook.getContact(timestamp);

        assertThat(contact).isNotNull();
        assertThat(contact.getId()).isEqualTo(timestamp);
    }

    @Test
    void getContactContactNotFound() {

        for (var contact: createContacts(5)) {
            contactsBook.addContact(contact);
        }

        Contact contact = contactsBook.getContact(40L);

        assertThat(contact).isNull();
    }

    @Test
    void isEmptyListIsNotEmpty() {
        for (var contact: createContacts(5)) {
            contactsBook.addContact(contact);
        }

        assertThat(contactsBook.isEmpty()).isFalse();
    }

    @Test
    void isEmpty() {

        assertThat(contactsBook.isEmpty()).isTrue();
    }
}