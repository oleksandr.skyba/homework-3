package com.playtika.learn.contacts.io;

import lombok.NonNull;

import java.io.PrintStream;

public class ConsoleOutput implements Output {

    private final PrintStream stream;

    public ConsoleOutput(@NonNull PrintStream printStream) {
        stream = printStream;
    }

    @Override
    public void printMessage(@NonNull String commandMessage, Object... args) {

        stream.println(String.format(commandMessage, args));
    }
}
