package com.playtika.learn.contacts.io;

import com.playtika.learn.contacts.exceptions.QuitException;
import org.javatuples.Pair;

public interface Input {

    Pair<Integer, Boolean> getInt() throws QuitException;

    Pair<Long, Boolean> getLong() throws QuitException;

    Pair<String, Boolean> getString() throws QuitException;

    String getQuitWord();
}
