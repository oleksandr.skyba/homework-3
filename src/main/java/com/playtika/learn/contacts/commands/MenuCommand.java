package com.playtika.learn.contacts.commands;

import com.playtika.learn.contacts.ContactsContainer;

public interface MenuCommand {

    ContactsContainer act(ContactsContainer contactsBook);

    CommandId getCommandId();

    String getCommandDescription();
}
