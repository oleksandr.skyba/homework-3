package com.playtika.learn.contacts;

import lombok.NonNull;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ContactsBook implements ContactsContainer {
    HashMap<Long, Contact> contacts;

    ContactsBook() {
        contacts = new HashMap<>();
    }

    @Override
    public long addContact(@NonNull Contact contact) {
        Instant nowInstant = Instant.now();
        Long timestampId = nowInstant.toEpochMilli();

        contact.setId(timestampId);

        contacts.put(timestampId, contact);

        return contact.getId();
    }

    @Override
    public List<Contact> getAllContacts() {
        return new ArrayList<>(contacts.values());
    }

    @Override
    public List<Contact> getContacts(Predicate<Contact> predicate) {
        return contacts.values()
                .stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }

    @Override
    public Boolean deleteContact(Long contactId) {
        return contacts.remove(contactId) != null;
    }

    @Override
    public Contact getContact(Long contactId) {
        return contacts.get(contactId);
    }

    @Override
    public boolean isEmpty() {
        return contacts.isEmpty();
    }


}
