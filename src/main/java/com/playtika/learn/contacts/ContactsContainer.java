package com.playtika.learn.contacts;

import lombok.NonNull;

import java.util.List;
import java.util.function.Predicate;

public interface ContactsContainer {
    long addContact(@NonNull Contact contact);

    List<Contact> getAllContacts();

    List<Contact> getContacts(Predicate<Contact> predicate);

    Boolean deleteContact(Long contactId);

    Contact getContact(Long contactId);

    boolean isEmpty();
}
